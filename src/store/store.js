import Vue from 'vue'
import Vuex from 'vuex'
import axios from '../plugins/axios'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    places: []
  },
  getters: {},
  mutations: {
    setPlaces (state, places) {
      state.places = places
    }
  },
  actions: {
    loadPlaces ({
      commit
    }) {
      axios.get('/api/places').then(function (response) {
        commit('setPlaces', response.data)
      })
    }
  }
})
