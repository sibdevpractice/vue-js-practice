import Vue from 'vue'
import VueRouter from 'vue-router'
import EditItemShop from './views/EditItemShop'
import Places from './views/Places'
import Logout from './views/Logout'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/',
      name: 'home'
    },
    {
      path: '/edit/:id',
      name: 'edit-shop',
      component: EditItemShop
    },
    {
      path: '/add/',
      name: 'add-shop',
      component: EditItemShop
    },
    {
      path: '/owner/places',
      name: 'places',
      component: Places
    },
    {
      path: '/logout/',
      name: 'logout',
      component: Logout
    }
  ]
})
